package com.webdb.webdb.repos;

import com.webdb.webdb.models.Posting;
import org.springframework.data.repository.CrudRepository;

public interface PostingRepository extends CrudRepository<Posting, Long> {

}
