package com.webdb.webdb.controllers;

import com.webdb.webdb.models.Posting;
import com.webdb.webdb.repos.PostingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Optional;


@Controller
public class BlogController {

    @Autowired
    private PostingRepository postingRepository;

    @GetMapping("/blog")
    public String blogMain (Model model){
        Iterable<Posting> posting = postingRepository.findAll();
        model.addAttribute("posts", posting);
        return "blogMain";
    }

    @GetMapping("/blog/add")
    public String blogAdd (Model model){
        return "blogAdd";
    }

    @PostMapping("/blog/add")
    public String blogStateAdd(@RequestParam String title,@RequestParam String fullText, Model model){
        Posting posting = new Posting(title,fullText);
        postingRepository.save(posting);
        return "redirect:/blog";
    }

    @GetMapping("/blog/{id}")
    public String blogDetails (@PathVariable(value = "id") long id, Model model){
        if(!postingRepository.existsById(id)){
            return "redirect:/blog";
        }
        Optional<Posting> posting = postingRepository.findById(id);
        ArrayList<Posting> result = new ArrayList<>();
        posting.ifPresent(result :: add);
        model.addAttribute("posts", result);
        return "blogDetails";
    }

    @GetMapping("/blog/{id}/edit")
    public String blogEdit (@PathVariable(value = "id") long id, Model model){
        if(!postingRepository.existsById(id)){
            return "redirect:/blog";
        }
        Optional<Posting> posting = postingRepository.findById(id);
        ArrayList<Posting> result = new ArrayList<>();
        posting.ifPresent(result :: add);
        model.addAttribute("post", result);
        return "blogEdit";
    }
    @PostMapping("/blog/{id}/edit")
    public String blogStateUpdate(@PathVariable(value = "id") long id, @RequestParam String title,@RequestParam String fullText, Model model){
       Posting posting = postingRepository.findById(id).orElseThrow();
       posting.setTitle(title);
       posting.setFullText(fullText);
       postingRepository.save(posting);
        return "redirect:/blog";
    }
    @PostMapping("/blog/{id}/remove")
    public String blogStateDelete(@PathVariable(value = "id") long id, Model model){
       Posting posting = postingRepository.findById(id).orElseThrow();
       postingRepository.delete(posting);
        return "redirect:/blog";
    }

}



